import * as React from "react";
import differenceInSeconds from "date-fns/differenceInSeconds";

async function fetchItems(setItems) {
  const response = await fetch(`/api/get-items`);
  const jsonResponse = await response.json();
  const items = jsonResponse.items;
  setItems(items);
}

const ListItems = () => {
  const now = new Date();
  const [items, setItems] = React.useState([]);

  React.useEffect(() => {
    fetchItems(setItems);
  }, []);

  // Not finished part for expired term countdown

  // function ExpiredTimer (array, delegate, delay)  {
 
  //   array.forEach(function (el, i) {
  //     setInterval(function() {
  //         return delegate( array[i]);
  
  //       }, delay);
  //   })
   
  // }

  // ExpiredTimer(items, function(obj) {Math.max((differenceInSeconds(new Date(obj.expiresIn), now))-1,0)},1000);
  


  return (
     
        <div className="content">
          <div className="container-fluid">
            <div className="row">
              {items.map((item) => (
                  <div className="col-lg-3 col-md-6 col-sm-6">
                    <div className="card card-stats pulse">
                      <div className="card-header card-header-warning card-header-icon">
                        <div className="card-icon">
                          <i className="fa fa-cutlery"></i>
                        </div>
                        <p className="card-category">Expires in</p>
                        <h3 className="card-title">
                          {Math.max((differenceInSeconds(new Date(item.expiresIn), now)),0)}
                          <small>S</small>
                        </h3>
                      </div>
                    <div className="card-footer">
                      <div className="stats">
                        <h4>{item.name}</h4>
                    </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
  );
};

export default ListItems;
 
