import * as React from "react";

class NewItemForm extends React.Component {

  constructor(props) {
    super(props);
    this.AddProduct = this.AddProduct.bind(this);
    this.state = {
      submitted: false,
      error: null
    };
  }

  async AddProduct() {

      let productInfo = {name : this.refs.name.value, expiresAfterSeconds: this.refs.expiresAfterSeconds.value};
      const requestOptions = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(productInfo)
      }
      fetch('/api/add-item', requestOptions)
        .then(async response => {
            const data = await response.json();

            if (!response.ok) {
                this.setState({submitted: true, error: 'Item with this name is already in the fridge'})
                alert('Item with this name is already in the fridge')
                return Promise.reject(error);
            }
            
        })
        .catch(error => {
            console.log(error)
        });
  }

  render() {

      const { submitted, error } = this.state;
      return (
        
      <div className="content">
        <div className="container-fluid">
          <div className="row">
            <div className="col-md-8">
              <div className="card">
                <div className="card-header card-header-primary">
                  <h4 className="card-title">Add something to fridge</h4>
                </div>
                <div className="card-body">
                  <form>
                    <div className="row">
                      <div className="col-md-5">
                        <div className="form-group">
                          <label className="bmd-label-floating">Item name</label>
                          <input
                              type="text"
                              className="form-control"
                              id="formItemName"
                              ref="name"
                              aria-describedby="itemNameHelp"
                          />
                          <small id="itemNameHelp" className="form-text text-muted">
                            We don't want more than one piece of the same food in our fridge
                          </small>
                        </div>
                      </div>
                      <div className="col-md-3">
                        <div className="form-group">
                          <label className="bmd-label-floating">Expires after(seconds)</label>
                          <input
                              type="number"
                              className="form-control"
                              id="formExpiresAfterSeconds"
                              ref="expiresAfterSeconds"
                              aria-label="Expires in"
                              aria-describedby="basic-addon2"
                              min="1" max="999"
                          />
                          <small id="itemNameHelp" className="form-text text-muted">
                            You can only put whole numbers between 1 and 999 (included)
                          </small>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <button className="btn btn-primary pull-right" onClick={this.AddProduct}>Add product</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    );
    }

}

export { NewItemForm }; 